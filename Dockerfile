FROM php:7.4.8-apache
LABEL Name=nlusearch Version=0.0.1 
RUN cp /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/rewrite.load

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" 

RUN apt-get -y update && pecl install xdebug \
    && docker-php-ext-enable xdebug
COPY docker/config/xdebug.ini $PHP_INI_DIR/conf.d/xdebug.ini
#RUN echo 'xdebug.default_enable=1' >> $PHP_INI_DIR/conf.d/xdebug.ini
#RUN echo 'xdebug.remote_enable=1' >> $PHP_INI_DIR/conf.d/xdebug.ini
#RUN echo 'xdebug.remote_port=9001' >> $PHP_INI_DIR/conf.d/xdebug.ini
#RUN echo 'xdebug.remote_handler=dbgp' >> $PHP_INI_DIR/conf.d/xdebug.ini
#RUN echo 'xdebug.remote_connect_back=0' >> $PHP_INI_DIR/conf.d/xdebug.ini
#RUN echo 'xdebug.remote_host=${PHP_XDEBUG_REMOTE_HOST}' >> $PHP_INI_DIR/conf.d/xdebug.ini 
#RUN echo 'xdebug.remote_autostart=1' >> $PHP_INI_DIR/conf.d/xdebug.ini
#RUN echo 'xdebug.remote_log=/var/www/html/xdebug.log' >> $PHP_INI_DIR/conf.d/xdebug.ini

CMD ["apache2ctl", "-k", "restart", "-D", "FOREGROUND"]